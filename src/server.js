const axiosCacheAdapter = require('axios-cache-adapter');
const config = require('config')

const express = require('express');
const bodyParser = require('body-parser');
const bcrypt = require('bcrypt');

const moment = require('moment');

// Importamos un módulo que hemos creado nosotros mismos.
// Los métodos exportados estarán disponibles en utils.<nombre del método> 
const utils = require('./utils');

const usersDB = require('./userDB');
const stPg = require('./storage_pg');

const { createLogger, format, transports } = require('winston');
const { combine, timestamp, label, printf } = format;
let id = 0;

const myFormat = printf(({ level, message, label, timestamp }) => {
  return `${timestamp} [${label}] ${level}: ${message}`;
});

/*
Aquí estamos creando un logger, esto es, un objeto que nos
va a permitir hacer logging, abstrayéndonos del destino de 
los mensajes. De esta forma, en nuestro código no tendremos que 
gestionar cómo abrir un fichero para guardar logs o cómo escribir
en pantalla. La librería también nos permite configurar
el nivel de log que va a cada destino
*/
const logger = createLogger({
  level: 'debug',
  format: combine(
    label({ label: 'main' }), timestamp(),
    myFormat
  ),
  defaultMeta: { service: 'user-service' },
  transports: [
    new transports.File({ filename: 'error.log', level: 'error' }),
    new transports.File({ filename: 'combined.log' }),
    new transports.Console()
  ]
});


/*
  Con esta librería implementamos una caché. Cuando hagamos
  peticiones de red a través de la caché, la librería nos devolverá 
  una respuesta anterior si la petición que hacemos es la misma. 
  De esta manera ahorramos ancho de banda
*/
const api = axiosCacheAdapter.setup({
  // `axios-cache-adapter` options
  cache: {
    maxAge: 0.5 * 60 * 1000
  }
})

let poiMap = {
  theater: [],
  beaches: [],
  council: []
}


let events = [];
const event1 = {
  name: 'nombre1',
  type: 'type1',
  date: 'date11',
  place: 'place'
};

events.push(event1);
/*
  La implementación de la autenticación que tenemos ahora mismo hace
  todos los pasos, entre ellos la generación de tokens. Otra alternativa sería
  usar `jwt` que es un mecanismo para generar tokens que contienen información
  sobre la sesión, como el rol o la caducidad, sin necesidad de tener que
  programarlo nosotros mismos.

  En el siguiente enlace podéis seguir un tutorial de cómo implementar la
  autenticación con jwt:

  https://solidgeargroup.com/refresh-token-autenticacion-jwt-implementacion-nodejs/?lang=es

  Otros links de interés:
  https://davidinformatico.com/jwt-express-js-passport/
  https://www.developerro.com/2019/03/12/jwt-api-authentication/

  Echad un vistazo también a la librería `passport`, que proporciona
  un montón de mecanismos de autenticación:
  https://www.npmjs.com/package/passport
*/
let validTokens = {}

const app = express();

/*
  Le estamos indicando a express que tiene que leer las variables 
  del BODY, esto es, el curso de las peticiones http que nos hagan.
  Aquí vendrá la información para el POST y el PUT.

  Esta forma de interactuar con express es un middleware. Se trata de funciones
  que se ejecutan en cada petición. En el ejemplo de CORS creamos un middleware
  desde cero
*/
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));


/*
  Con esta línea le estamos diciendo a express que
  sirva ficheros de la carpeta 'public'. Lo usamos
  para poder tener un servidor web que sirve una página que 
  hace peticiones a nuestra API, y no tener problemas de CORS.
  Los problemas de CORS los tuvimos cuando lanzamos un
  servidor en otro puerto con `python -m SimpleHTTPServer`
*/
app.use(express.static('public'));

/*
  Obtenemos del fichero de configuración los dominios
  a los que vamos a permitir el acceso a la API
*/
const domainCors = config.get('domainCors');


/*
  Configuración de CORS. 

  Como decíamos más arriba, esto es un middleware, es decir,
  una función que se va a ejecutar en cada petición. Puede haber 
  un número indefinido de ellos y su finalidad es modificar el contenido
  de los objetos req y res. En este caso estamos modificando el objeto
  de respuesta para que incluya cabeceras para indicarle cuáles son 
  los dominios desde los que se permite acceder a la API.

  En el caso anterior, lo que hacíamos era indicarle que leyera la
  información del body

  Notad la llamada a 'next()'. Es necesario llamar a esta función para
  encadenar todos los middlewares definidos.
*/
app.use(function (req, res, next) {
  res.header("Access-Control-Allow-Origin", domainCors.join(','));
  res.header("Access-Control-Allow-Headers", "Origin, X-Requested-With, Content-Type, Accept");
  next();
});

/*
  Esto es una constante para usar con bcrypt, esto es, 
  la librería que nos calcula el hash de la password (para
  que no haya que almacenarla en claro, lo cual sería inseguro)
 */
const BCRYPT_SALT_ROUNDS = 12;

app.post('/register', function (req, res, next) {
  const username = req.body.username;
  const password = req.body.password;

  // usamos bcrypt para calcular el hash de la
  // password, esto es, para transformar la password real
  // en otro conjunto de caracteres a partir del cual
  // no se pueda averiguar cuál es la password original
  bcrypt.hash(password, BCRYPT_SALT_ROUNDS)
    .then(function (hashedPassword) {
      return stPg.saveUser(username, hashedPassword);
    })
    .then(function () {
      res.send();
    })
    .catch(function (error) {
      res.status(400).send();
      console.log("Error saving user: ");
      console.log(error);
      next();
    });
});


app.post('/login', function (req, res, next) {
  const username = req.body.username;
  const password = req.body.password;

  const createToken = () => {
    // para esto podríamos haber usado alguna librería como:
    // https://www.npmjs.com/package/rand-token
    return Math.random().toString(36).substring(2, 15) + Math.random().toString(36).substring(2, 15)
  }

  stPg.getUserByUsername(username)
    .then(function (user) {
      // le preguntamos a bcrypt si las passwords que nos pasan(en claro)
      // y la que tenemos almacenada (hasheada) son iguales. Notad que
      // no podemos compararlas con el operador de igualdad (==), ya que 
      // la password almacenada ha sido transformada. Bcrypt sabe cómo
      // realizar dicha comparación
      return bcrypt.compare(password, user.password);
    })
    .then(function (samePassword) {
      if (!samePassword) {
        res.status(403).send();
      }

          let token = createToken();

      validTokens[token] = {
        expires: moment().add(30, 's'),
        rol: 'user'
      }

      res.json({ token: token });
    })
    .catch(function (error) {
      console.log("Error authenticating user: ");
      console.log(error);
      res.status(403).send();
      next();
    });
});


app.get('/events', (req, res) => {

  res.send(events);
});

app.post('/events', (req, res) => {


  const name= req.body['name'];
  const type = req.body['type'];
  const date = req.body['date'];
  const place = req.body['place'];
  const desciption = req.body['description']

  let eventdata = {};

  eventdata['name'] = name;
  eventdata['type'] = type;
  eventdata['date'] = date;
  eventdata['place'] = place;
  eventdata['description'] = desciption;

  for (let i = 0; i < events.length; i++) {
    if (events[i].name == eventdata.name &&
      events[i].type == eventdata.type &&
      events[i].date == eventdata.date &&
      events[i].place == eventdata.place) {
      res.status(409).send()
      return
    }
  }
  for (let value of Object.values(eventdata)) {
    if (value === undefined || value.trim().length === 0) {
      res.status(400).send();
      return
    }
  }

  events.push(eventdata);

  res.send(eventdata);

});

// app.post('/poi/:collection', (req, res) => {
//   const collection = req.params['collection'];

//   if (poiMap[collection] === undefined) {
//     res.status(404).send();
//     return
//   }

//   const concello = req.body['concello'];
//   const provincia = req.body['provincia'];
//   const web = req.body['web'];
//   const nome = req.body['nome'];
//   const coordenadas = req.body['coordenadas'];

//   let poiData = {};

//   poiData['concello'] = concello;
//   poiData['provincia'] = provincia;
//   poiData['web'] = web;
//   poiData['nome'] = nome;
//   poiData['coordenadas'] = coordenadas;

//   for (let value of Object.values(poiData)) {
//     if (value === undefined || value.trim().length === 0) {
//       res.status(400).send('hola');
//       return
//     }
//   }

//   poiData['id'] = id;

//   poiData.data = {};

//   for (let i = 0; i < poiMap[collection].length; i++) {
//     if (poiMap[collection][i].concello == poiData.concello &&
//       poiMap[collection][i].provincia == poiData.provincia &&
//       poiMap[collection][i].web == poiData.web &&
//       poiMap[collection][i].nome == poiData.nome) {
//       res.status(409).send()
//       return
//     }
//   }

//   for (let key of Object.keys(req.body)) {
//     if (poiData[key] === undefined) {
//       const value = req.body[key].trim();

//       if (value.length === 0) {
//         res.status(400).send('adios');
//         return;
//       }

//       poiData.data[key] = value;
//     }
//   }


//   poiMap[collection].push(poiData);

//   id++;

//   res.send(poiData.id.toString());


// });


// app.get('/poi/:collection', (req, res) => {
//   const collection = req.params['collection'];

//   if (poiMap[collection] === undefined) {
//     res.status(404).send();
//     return;
//   }
//   res.send(poiMap[req.params.collection]);
// });


// app.delete('/poi/:collection/:id', (req, res) => {
//   let id = parseInt(req.params['id']);
//   const collection = req.params['collection'];

//   if (poiMap[collection] === undefined) {
//     res.status(404).send();
//     return
//   }

//   for (let i = 0; i < poiMap[collection].length; i++) {
//     if (poiMap[collection][i]['id'] === id) {
//       poiMap[collection].splice(i, 1);
//       res.status(204).send()
//       return
//     }
//   }
//   res.status(404).send();

// });

// app.delete('/poi/:collection', (req, res) => {
//   let collection = req.params['collection'];

//   if (poiMap[collection] === undefined) {
//     res.status(404).send();
//     return;
//   }
//   if (poiMap[collection].length > 0) {
//     res.status(409).send();
//     return;
//   }

//   delete poiMap[collection];
//   res.send();
// });


const port = config.get('server.port');

app.listen(port, function () {
  logger.info(`Starting points of interest application listening on port ${port}`);
});
